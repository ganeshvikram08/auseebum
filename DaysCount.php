<?php
include 'dateFunctions.php';
/*
Agorithm steps

1. check exact 2 parameters are received
2. check both dates are valid
3. date2 > date1
4. check date range between 1901-2999 or not
5. calculate number of days
6. check with given testcase
*/

class DaysCount extends DateFunctions{
	
	private $startDateRange = DATE_RANGE_START;
	private $endDateRange   = DATE_RANGE_END;
	private $startDate;
	private $endDate;
	
	function __construct($argv) {

		//Validate number of arguement
		if(count($argv)!=3){
			$this->validationFailed('Exact 2 arguements are required');
		}
		
		$this->startDate = str_replace('/','-',$argv[1]);
		$this->endDate   = str_replace('/','-',$argv[2]);
		
    }
	
	private function validateInput($inputStartDate,$inputEndDate){
		
		//Check start date valid
		if(!$this->checkDateValid($inputStartDate)){
			$this->validationFailed('Startdate '.$inputStartDate.' is invalid.(Allowed date format dd/mm/yyyy)');
		}
		
		//Check end date valid
		if(!$this->checkDateValid($inputEndDate)){
			$this->validationFailed('Enddate '.$inputEndDate.' is invalid.(Allowed date format dd/mm/yyyy)');
		}
		
		//Check startdate less than end date
		if(!$this->checkDateRange($this->startDateRange,$this->endDateRange,$inputStartDate)){
			$this->validationFailed('Allowed start date range between '.$this->startDateRange.' to '.$this->endDateRange);
		}
		
		//Check startdate less than end date
		if(!$this->checkDateRange($this->startDateRange,$this->endDateRange,$inputEndDate)){
			$this->validationFailed('Allowed end date range between '.$this->startDateRange.' to '.$this->endDateRange);
		}
		
		//Check startdate less than end date if not swap dates
		if(!$this->checkStdateLessthanEnddate($inputStartDate,$inputEndDate)){
			$tempDate = $this->startDate;
			$this->startDate = $this->endDate;
			$this->endDate = $tempDate;
		}
	}
	
	private function validationFailed($str){
		if (defined('TEST_MODE') && TEST_MODE==1) {
			return $str;
		}else{
			die('Validation Failed: '.$str);
		}
	}
	
	public function countDays(){
		$this->validateInput($this->startDate,$this->endDate);
		return $this->CalculateDays($this->startDate,$this->endDate).' days';
	}

}
?>