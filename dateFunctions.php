<?php
class DateFunctions {
	
	//Check date valid or not
	protected function checkDateValid($str){
		$dateArr = array_map('intval', explode('-', $str));
		if(count($dateArr)!=3)return false;
		return checkdate($dateArr[1], $dateArr[0], $dateArr[2]);
	}

	//Check start date lessthan end date or not
	protected function checkStdateLessthanEnddate($startDate,$EndDate){
		return (strtotime($EndDate)>strtotime($startDate))?true:false;
	}

	//Check date in given range
	protected function checkDateRange($startRange,$endRange,$date){
		return (strtotime($date)>=strtotime($startRange) && strtotime($date)< strtotime($endRange))?true:false;
	}

	//Calculate total days between dates
	protected function CalculateDays($startDate,$EndDate){
		return (round((strtotime($EndDate)-strtotime($startDate)) / (60 * 60 * 24)))-1; // -1 reducing started day from calculation
	}
}
?>