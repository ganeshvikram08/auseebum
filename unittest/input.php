<?php
define('TEST_MODE', '1');

/**
	element 0 - dummy value
	element 1 - start date
	element 2 - end date
	element 3 - expected output

**/
$input = [

	[0=>'test-projectDays.php',1=>'02/06/1983',2=>'22/06/1983',3=>'19 days'],
	[0=>'test-projectDays.php',1=>'03/01/1989',2=>'03/08/1983',3=>'1979 days'],
	[0=>'test-projectDays.php',1=>'04/07/1984',2=>'25/12/1984',3=>'173 days'],

];
 
?>