<?php
include 'DaysCount.php';
include 'featureProperties.php';
include 'input.php';

$pass = 0;
$fail = 0;

//process test input array
foreach($input as $v){
	
	$output = array_pop($v);
	$obj = new DaysCount($v);
	$result = $obj->countDays();

	if($result==$output){
		$pass++;
		print_r('Test passed. startdate: '.$v[1].'  enddate: '.$v[2].'  output: '.$output."\n");
	}else{
		$fail++;
		print_r('Test failed. startdate: '.$v[1].'  enddate: '.$v[2].'  expected-output:  '.$output.'  received-output: '.$result."\n");
	}
	
}

print_r('Total test: '.count($input).'  Test passed: '.$pass .'  Test failed: '.$fail);
?>