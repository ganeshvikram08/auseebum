## Execution tips

1. Can execute only in command-line based system.
2. Supporting version php 7+
3. Example commands for execution like follows
	'php projectDays.php 03/01/1989 03/08/1983'
4. Two arguments required. 
5. Both arguments should be valid date and allowed format is 'dd/mm/yyyy'
6. Both date range must between 01/01/1901 to 31/12/2999  and date range declared in featureProperties.php you can change as per need.


## Unit test execution tips

1. unittest folder contains unit test file 'test-projectDays.php'
2. unittest inputs are declared in 'unittest\input.php' and you can change with your own inputs with in the same format.
3. Command to execute unit test like follows
	'php unittest\test-projectDays.php'


	